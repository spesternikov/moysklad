package ru.moysklad.testtask.model;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(schema = "moysklad", name = "bank_account")
public class BankAccount implements Serializable {

    @Id
    @Pattern(regexp = "^[0-9]{5}$", message = "id must be 5 digits")
    @Column(name = "id", unique = true)
    private String id;

    @DecimalMin(value = "0", message = "balance must be positive or 0")
    @Column(name = "balance")
    private BigDecimal balance = new BigDecimal(0);

    public BankAccount() {
    }

    public BankAccount(@Valid @Pattern(regexp = "^[0-9]{5}$", message = "id must be 5 digits") String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
