package ru.moysklad.testtask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.moysklad.testtask.dto.Balance;
import ru.moysklad.testtask.model.BankAccount;
import ru.moysklad.testtask.repository.BankAccountRepository;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

@Service
public class BankAccountService {

    private BankAccountRepository bankAccountRepository;

    @Autowired
    public BankAccountService(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public boolean create(String id) {
        if (bankAccountRepository.findById(id).isPresent()) {
            return false;
        }

        BankAccount bankAccount = new BankAccount(id);

        bankAccountRepository.save(bankAccount);

        return true;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deposit(String id, BigDecimal deposit) {
        BankAccount bankAccount = bankAccountRepository.findById(id)
                .orElseThrow(() ->  new NoSuchElementException("Bank account not found"));

        BigDecimal balance = bankAccount.getBalance();
        bankAccount.setBalance(balance.add(deposit));

        bankAccountRepository.save(bankAccount);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void withdraw(String id, BigDecimal withdraw) {
        BankAccount bankAccount = bankAccountRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Bank account not found"));

        BigDecimal balance = bankAccount.getBalance();
        balance = balance.subtract(withdraw);

        if (balance.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Balance after withdraw less than 0");
        }

        bankAccount.setBalance(balance);

        bankAccountRepository.save(bankAccount);
    }


    public Balance getBalance(String id) {
        BankAccount bankAccount = bankAccountRepository.findById(id)
                .orElseThrow(() -> {throw new NoSuchElementException("Bank account not found");});
        BigDecimal balance = bankAccount.getBalance();
        return new Balance(balance);
    }
}
