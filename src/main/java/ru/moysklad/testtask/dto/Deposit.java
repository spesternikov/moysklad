package ru.moysklad.testtask.dto;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class Deposit {
    @DecimalMin(value = "0", message = "deposit must be positive or 0")
    private BigDecimal deposit;

    public BigDecimal getDeposit() {
        return deposit;
    }

    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }
}
