package ru.moysklad.testtask.dto;

import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;

public class Withdraw {
    @DecimalMin(value = "0", message = "withdraw must be positive or 0")
    private BigDecimal withdraw;

    public BigDecimal getWithdraw() {
        return withdraw;
    }

    public void setWithdraw(BigDecimal withdraw) {
        this.withdraw = withdraw;
    }
}
