package ru.moysklad.testtask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import ru.moysklad.testtask.dto.Balance;
import ru.moysklad.testtask.dto.Deposit;
import ru.moysklad.testtask.api.Response;
import ru.moysklad.testtask.dto.Withdraw;
import ru.moysklad.testtask.service.BankAccountService;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.NoSuchElementException;

@RestController
@Validated
public class BankAccountController {
    private final String ID_VALID_REGEXP = "^[0-9]{5}$";
    private final String ID_VALID_REGEXP_MGS = "id must be 5 digits";

    private BankAccountService bankAccountService;

    @Autowired
    public BankAccountController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @ExceptionHandler({ ConstraintViolationException.class, MethodArgumentTypeMismatchException.class, MethodArgumentNotValidException.class })
    public ResponseEntity<Response> handleBadRequest(Exception ex) {
        Response response = new Response(true, null, ex.getMessage());
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ NoSuchElementException.class })
    public ResponseEntity<Response> handleNotFound(Exception ex) {
        Response response = new Response(true, null, ex.getMessage());
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ IllegalArgumentException.class })
    public ResponseEntity<Response> handleForbidden(Exception ex) {
        Response response = new Response(true, null, ex.getMessage());
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Response> handleInternal(Exception ex) {
        Response response = new Response(true, null, ex.getMessage());
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping("/bankaccount/{id}")
    public ResponseEntity<Response> createAccount(@PathVariable @Valid @Pattern(regexp = ID_VALID_REGEXP, message = ID_VALID_REGEXP_MGS) String id) {

        if (bankAccountService.create(id)) {
            Response response = new Response(false, null, null);
            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
        } else {
            Response response = new Response(true, null, "Bank account already exists");
            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping("/bankaccount/{id}/deposit")
    public ResponseEntity<Response> deposit(@PathVariable @Valid @Pattern(regexp = ID_VALID_REGEXP, message = ID_VALID_REGEXP_MGS) String id,
                                            @Valid @RequestBody Deposit deposit) {
        bankAccountService.deposit(id, deposit.getDeposit());
        Response response =  new Response(false, null, null);
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping("/bankaccount/{id}/withdraw")
    public ResponseEntity<Response> withdraw(@PathVariable @Valid @Pattern(regexp = ID_VALID_REGEXP, message = ID_VALID_REGEXP_MGS) String id,
                                            @Valid @RequestBody Withdraw withdraw) {
        bankAccountService.withdraw(id, withdraw.getWithdraw());
        Response response =  new Response(false, null, null);
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/bankaccount/{id}/balance")
    public ResponseEntity<Response> getBalance(@PathVariable @Valid @Pattern(regexp = ID_VALID_REGEXP, message = ID_VALID_REGEXP_MGS) String id) {
        Balance balance = bankAccountService.getBalance(id);
        Response response =  new Response(false, balance, null);
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
}
