package ru.moysklad.testtask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.moysklad.testtask.model.BankAccount;


@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, String> {

}
