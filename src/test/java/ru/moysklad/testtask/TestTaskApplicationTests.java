package ru.moysklad.testtask;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.moysklad.testtask.controller.BankAccountController;

@TestPropertySource(locations= "classpath:test.yaml")
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableAutoConfiguration
public class TestTaskApplicationTests {

	@Autowired
	private BankAccountController bankAccountController;

	@Test
	public void contextLoads() {
		assertThat(bankAccountController).isNotNull();
	}

}
