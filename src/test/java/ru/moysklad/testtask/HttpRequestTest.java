package ru.moysklad.testtask;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import ru.moysklad.testtask.dto.Deposit;
import ru.moysklad.testtask.api.Response;
import ru.moysklad.testtask.dto.Withdraw;
import java.math.BigDecimal;
import java.util.HashMap;

@TestPropertySource(locations= "classpath:test.yaml")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testCreateFailed() {
        Response response = restTemplate.postForObject("/bankaccount/bad_id",
                null, Response.class);

        assertThat(response.isError()).isTrue();
        assertThat(response.getMessage()).isNotNull();
    }

    @Test
    public void testDepositAndWithdraw() {

        restTemplate.postForLocation("/bankaccount/00000", null);

        Deposit deposit = new Deposit();
        deposit.setDeposit(new BigDecimal(100));


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity httpEntity = new HttpEntity<>(deposit, headers);
        ResponseEntity<Response> responseEntity = restTemplate.exchange("/bankaccount/00000/deposit",
                HttpMethod.PUT, httpEntity, Response.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        Response response = responseEntity.getBody();

        assertThat(response).isNotNull();

        assertThat(response.isError()).isFalse();
        assertThat(response.getMessage()).isNull();
        assertThat(response.getData()).isNull();

        Withdraw withdraw = new Withdraw();
        withdraw.setWithdraw(new BigDecimal(100));

        httpEntity = new HttpEntity<>(withdraw, headers);

        responseEntity = restTemplate.exchange("/bankaccount/00000/withdraw",
                HttpMethod.PUT, httpEntity, Response.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        response = responseEntity.getBody();

        assertThat(response).isNotNull();

        assertThat(response.isError()).isFalse();
        assertThat(response.getMessage()).isNull();
        assertThat(response.getData()).isNull();
    }

    @Test
    public void testWithdrawFailed() {
        restTemplate.postForLocation("/bankaccount/00000", null);

        Response response = restTemplate.getForObject("/bankaccount/00000/balance",
                Response.class);

        HashMap<String, Object> data = (HashMap<String, Object>)response.getData();

        Integer balance = (Integer) data.get("balance");

        Withdraw withdraw = new Withdraw();
        withdraw.setWithdraw(new BigDecimal(100).add(new BigDecimal(balance.intValue())));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity httpEntity = new HttpEntity<>(withdraw, headers);

        ResponseEntity<Response> responseEntity = restTemplate.exchange("/bankaccount/00000/withdraw",
                HttpMethod.PUT, httpEntity, Response.class);

        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);

        response = responseEntity.getBody();

        assertThat(response).isNotNull();

        assertThat(response.isError()).isTrue();
        assertThat(response.getMessage()).isNotNull();
        assertThat(response.getData()).isNull();

    }


    @Test
    public void testBalance() {
        restTemplate.postForLocation("/bankaccount/00000", null);
        Response response = restTemplate.getForObject("/bankaccount/00000/balance",
                Response.class);

        assertThat(response.isError()).isFalse();
        assertThat(response.getMessage()).isNull();
        assertThat(response.getData()).isNotNull();
    }

    @Test
    public void testBalanceFailed() {
        Response response = restTemplate.getForObject("/bankaccount/bad_id/balance",
                Response.class);

        assertThat(response.isError()).isTrue();
        assertThat(response.getMessage()).isNotNull();
    }

}
