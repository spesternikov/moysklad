FROM postgres:latest

ENV LANG en_US.UTF-8 
ENV LANGUAGE en_US:ru
ENV LC_ALL en_US.UTF-8
ENV TZ Europe/Moscow

USER postgres
WORKDIR /var/lib/postgresql

RUN mkdir /var/lib/postgresql/schema

COPY ./schema/db.sql /var/lib/postgresql/schema/db.sql
COPY ./schema/ddl.sql /var/lib/postgresql/schema/ddl.sql

RUN initdb -D /var/lib/postgresql/data_dir -E UTF8 --locale=en_US.UTF8

COPY ./pg_hba.conf /var/lib/postgresql/data_dir/pg_hba.conf 

RUN pg_ctl -D /var/lib/postgresql/data_dir -w start && \
    psql -d postgres -U postgres -W postgres -f ~/schema/db.sql && \
    psql -d moysklad -U postgres -W postgres -f ~/schema/ddl.sql && \
    pg_ctl -D /var/lib/postgresql/data_dir -m fast -w stop

EXPOSE 5432

CMD ["postgres", "-D", "/var/lib/postgresql/data_dir"]