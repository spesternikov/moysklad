FROM openjdk:8u181-jre-stretch

ENV LANG en_US.UTF-8 
ENV LANGUAGE en_US:ru
ENV LC_ALL en_US.UTF-8
ENV TZ Europe/Moscow

RUN apt-get update -y && \
    apt-get install -y maven && \
    apt-get clean all -y

RUN useradd username -d /home/username -m -b /bin/bash

USER username
WORKDIR /home/username

COPY ./target/test-task-0.0.1-SNAPSHOT.jar /home/username/test-task-0.0.1-SNAPSHOT.jar
COPY ./start.sh /home/username/start.sh

EXPOSE 8080

CMD ["/bin/bash", "/home/username/start.sh"]