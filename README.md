# Тестовое задание для компании "Мой Склад"

## Введение

Приветствую тебя ${%username%}. Вот настал момент моей попытки перейти с моего стэка на Java.
Попыток было много, по пути к Java пришлось освоить C/C++, Python, Node.js, Golang (каюсь PHP тоже было)... 
но путь осилит идущий. И вот я накидал за несколько часов простенький REST-сервис.

## Сборка

Как ты уже навервное догодался, что для сборки моего замечательного задания используется maven. 
Т.к. для тестов необходим рабочий инстанс PostgreSQL, то рекомендуется отключать тесты во время сборки.

Для сборки необходимо выполнить следующую команду:

```
    $ mvn clean package -Dmaven.test.skip=true
```

## Тесты

У этого замечательного приложения есть тесты, для выполнения которых необходим инстанс PostgreSQL.

Тесты можно запустить двумя способами:

* С передачей параметров через переменные окружения:

```
    $ SPRING_DATASOURCE_URL="jdbc:postgresql://localhost:5432/moysklad" SPRING_DATASOURCE_USERNAME="moysklad" SPRING_DATASOURCE_PASSWORD="moysklad" mvn test
```

SPRING_DATASOURCE_URL - урл доступа к БД;

SPRING_DATASOURCE_USERNAME - имя пользователя;

SPRING_DATASOURCE_PASSWORD - пароль.


* С передачей параметров через аргументы командной строки:

```
    $ mvn test -Dspring.datasource.url="jdbc:postgresql://localhost:5432/moysklad" -Dspring.datasource.username="moysklad" -Dspring.datasource.password="moysklad"
```

spring.datasource.url - урл доступа к БД;

spring.datasource.username - имя пользователя;

spring.datasource.password - пароль.

## Запуск

Запуcтить приложение можно также 2 способами:

* С передачей параметров через переменные окружения:

```
    $ SERVER_PORT=8080 SERVER_HOST="127.0.0.1" SPRING_DATASOURCE_URL="jdbc:postgresql://localhost:5432/moysklad" SPRING_DATASOURCE_USERNAME="moysklad" SPRING_DATASOURCE_PASSWORD="moysklad" mvn spring-boot:run
```

или

```
    $ SERVER_PORT=8080 SERVER_HOST="127.0.0.1" SPRING_DATASOURCE_URL="jdbc:postgresql://localhost:5432/moysklad" SPRING_DATASOURCE_USERNAME="moysklad" SPRING_DATASOURCE_PASSWORD="moysklad" java -jar test-task-0.0.1-SNAPSHOT.jar
```

SERVER_PORT - порт http-сервера;

SERVER_HOST - хост http-сервера;

SPRING_DATASOURCE_URL - урл доступа к БД;

SPRING_DATASOURCE_USERNAME - имя пользователя;

SPRING_DATASOURCE_PASSWORD - пароль.

* С передачей параметров через аргументы командной строки:

```
    $ mvn spring-boot:run -Dserver.port=8080 -Dserver.host="127.0.0.1"  -Dspring.datasource.url="jdbc:postgresql://localhost:5432/moysklad" -Dspring.datasource.username="moysklad" -Dspring.datasource.password="moysklad"
```

или

```
    $ java -jar test-task-0.0.1-SNAPSHOT.jar --server.port=8080 --server.host="127.0.0.1"  --spring.datasource.url="jdbc:postgresql://localhost:5432/moysklad" --spring.datasource.username="moysklad" --spring.datasource.password="moysklad"
```

server.port - порт http-сервера;

server.host - хост http-сервера;

spring.datasource.url - урл доступа к БД;

spring.datasource.username - имя пользователя;

spring.datasource.password - пароль.

## Схема данных

Схема данных описана в двух файлах `db.sql` и `ddl.sql` в директории schema.

Для подготовки схемы данных необходимо выполнить следующие команды:

```
    $ psql -h <dbhost> -p <dbport> -d postgres -U postgres -W postgres -f db.sql
    $ psql -h <dbhost> -p <dbport> -d moysklad -U postgres -W postgres -f ddl.sql
```

## Описание REST API

Описание REST API доступно в диерктории doc в файле `REST.md`

## Docker

Для сборки контейнеров в корне лежит 2 докерфайла: `postgres.Dockerfile` и `test-app.Dockerfile`.

Для сборки контейнеров в корне также присутствуют файлы `pg_hba.conf` и `start.sh`. Также, для сборки используются файлы в директории schema.

Также, в репозитории есть `docker-compose.yml`, который позволяет рабочий проект одной командой  `docker-compose up -d`. Если у вас занят порт 8080, то необходимо сменить значение в конфиге ports в `docker-compose.yml`. 

Например:

ports:
  - 7778:8080

Где 7778 порт на вашем хосте.

Для остановки приложения необходимо выполнить docker-compose down

Накати в ридми