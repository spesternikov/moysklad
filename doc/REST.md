# Описание REST API

## Общие соглашения

1. Запросы и ответы передаются в JSON-объектах
2. Ответ на любой запрос содерджит три поля:
    * error (Boolean) - признак наличия ошибки
    * message (String) - описание ошибки (опционально)
    * data (Object) - объект с данным (опционально)
    
## API

### Создание нового счёта

Метод : POST

Url:

```
    http://<host>:<port>/bankaccount/{id}
```

Параметры запроса:

* id - 5-ти значное число

Ответ:

```
    {
        "error": Number,
        "message": String/Null
    }
```

HTTP status codes: 200, 400, 404, 500

### Внесение суммы на счёт

Метод: PUT

Url:

```
    http://<host>:<port>/bankaccount/{id}/deposit
```

Параметры запроса:

* id

```
    {
        "deposit": Number
    }
```

* deposit - положительное число

Ответ:

```
    {
        "error": Number,
        "message": String/Null
    }
```

HTTP status codes: 200, 400, 404, 500

### Снятие суммы со счёта

Метод: PUT

Url:

```
    http://<host>:<port>/bankaccount/{id}/withdraw
```

Параметры запроса:

* id

```
    {
        "withdraw": Number
    }
```

* withdraw - положительное число

Ответ:

```
    {
        "error": Number,
        "message": String/Null
    }
```

HTTP status codes: 200, 400, 403, 404, 500

### Получение баланса счёта

Метод: GET

Url:

```
    http://<host>:<port>/bankaccount/{id}/balance
```

Параметры запроса:

* id

Ответ:

```
    {
        "error": Number,
        "message": String/Null,
        "data": Balance
    }
```

Balance:

```
    {
        "balance": Number
    }
```

HTTP status codes: 200, 400, 404, 500
