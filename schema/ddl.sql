CREATE SCHEMA AUTHORIZATION moysklad;

DROP TABLE moysklad.bank_account;

CREATE TABLE moysklad.bank_account (
  id varchar(5) PRIMARY KEY CHECK (id ~ '^[0-9]{5}$'),
  balance NUMERIC NOT NULL DEFAULT 0 CHECK (balance >= 0)
);

GRANT ALL PRIVILEGES ON moysklad.bank_account TO moysklad;